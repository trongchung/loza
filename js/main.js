

// menu toggle
$(function() {
    var html = $('html, body'),
        navContainer = $('.mainMenu__navCont'),
        navToggle = $('.nav-toggle'),
        navToggle2 = $('.open-menu'),
        navDropdownToggle = $('.has-dropdown');
        overlay =$("<div class='overlay'></div> ");
        overlay2 =$("<div class='overlay2'></div> ");

    // Nav toggle
    navToggle.on('click', function(e) {
        overlay.toggle();
        var $this = $(this);
        e.preventDefault();
        $this.toggleClass('is-active');
        navContainer.toggleClass('is-visible');
        html.toggleClass('nav-open');
    });



    $( "body" ).prepend(overlay);
    overlay.click(function(){
        navToggle.trigger('click');
    })
    navToggle2.click(function(){
        navToggle.trigger('click');
    })


    $( "body" ).prepend(overlay2);
    overlay2.click(function(){
         $(this).toggle();
    })
    // Nav dropdown toggle
    navDropdownToggle.on('click', function() {
        var $this = $(this);
        $this.toggleClass('is-active').siblings().removeClass('is-active');
        // if(!$(this).children('ul').is(":visible"))
        // {
        //   $(this).children('ul').slideDown();
        // }
        if ($this.children('ul').hasClass('open-nav')) {
              $this.children('ul').removeClass('open-nav');
              $this.children('ul').slideUp(350);
          }
        else {
          $this.parent().parent().find('li .nav-dropdown').removeClass('open-nav');
          $this.parent().parent().find('li .nav-dropdown').slideUp(350);
          $this.children('ul').toggleClass('open-nav');
          $this.children('ul').slideToggle(350);
        }
    });

    // Prevent click events from firing on children of navDropdownToggle
    navDropdownToggle.on('click', '*', function(e) {
        e.stopPropagation();
    });


});

 // style img

$(function() {
    render_size();
    var url = window.location.href;
    $('.mainMenu__navCont li  a').parent().removeClass('active');
    $('.mainMenu__navCont li  a[href="' + url + '"]').parent().addClass('active');
});

$(window).resize(function() {
    render_size();
});

function render_size() {
    var h_7714 = $('.h_7714 img').width();
    $('.h_7714 img').height(Math.ceil(0.7714 * parseInt(h_7714)));

    var h_5 = $('.h_5 img').width();
    $('.h_5 img').height(Math.ceil(0.5 * parseInt(h_5)));

}

 // scroll add class
if (window.innerWidth > 768) {
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 100) {
            $('.sticky-header').addClass('fixed');
        } else{
            $('.sticky-header').removeClass('fixed');
        }
    });
}
if (window.innerWidth > 320) {
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 100) {
            $('.sticky-header').addClass('clearfix');
        } else {
            $('.sticky-header').removeClass('clearfix');
        }
    });
}



// btn_search
$(function () {
      // search dropdown button
      $('.btn_search').click(function (e) {
          overlay2.toggle();
          e.preventDefault();
          $(this).parents('.search_drop').find('.form_search').toggleClass('open')
      })
      $(document).click(function (event) {
          // Check if clicked outside target
          if (!($(event.target).closest(".search_drop").length)) {
              // Hide target
              $(".form_search").removeClass('open');

          }

      });
    });




//scroll to top button
$(function() {
    $("a[href='#top']").click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, "slow");
        return false;
    });
}, 0);

$(window).scroll(function () {
    if ($(window).scrollTop() >=200) {
        $('#go_top').show();
    }
    else {
        $('#go_top').hide();
    }
});



// slider



 $(function() {
        $(".slider_main").owlCarousel({
        items: 1,
        responsive: {
            1200: { item: 1, },// breakpoint from 1200 up
            992: { items: 1, },
            768: { items: 1, },
            480: { items: 1, },
            0: { items: 1, }
        },
        rewind: false,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 5000,
        smartSpeed: 1000, //slide speed smooth
        dots: false,
        dotsEach: false,
        loop: false,
        nav: false,
        navText: ['<i class="fa fa-angle-left arrow-slider"></i>','<i class="fa fa-angle-right arrow-slider"></i>'],
        margin: 0,
        center: false,
    });
        $(".slider-sale").owlCarousel({
        items: 4,
        responsive: {
            1200: { item: 4, },// breakpoint from 1200 up
            992: { items: 3, },
            768: { items: 3, },
            480: { items: 1, },
            0: { items: 1, }
        },
        rewind: false,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 5000,
        smartSpeed: 1000, //slide speed smooth
        dots: false,
        dotsEach: false,
        loop: false,
        nav: true,
        navText: ['<i class="fa fa-angle-left arrow-slider"></i>','<i class="fa fa-angle-right arrow-slider"></i>'],
        margin: 30,
        center: false,
    });
        $(".slider-news").owlCarousel({
        items: 4,
        responsive: {
            1200: { item: 4, },// breakpoint from 1200 up
            992: { items: 3, },
            768: { items: 3, },
            480: { items: 1, },
            0: { items: 1, }
        },
        rewind: false,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 5000,
        smartSpeed: 1000, //slide speed smooth
        dots: false,
        dotsEach: false,
        loop: false,
        nav: false,
        navText: ['<i class="fa fa-angle-left arrow-slider"></i>','<i class="fa fa-angle-right arrow-slider"></i>'],
        margin: 20,
        center: false,
    });
     $(".slider_mb").owlCarousel({
         items: 4,
         rewind: false,
         autoplay: true,
         autoplayHoverPause: false,
         autoplayTimeout: 4000,
         smartSpeed: 1000, //slide speed smooth
         dots: false,
         dotsEach: false,
         loop: false,
         nav: true,
         navText: ['<i class="fa fa-angle-left arrow-slider-mb"></i>','<i class="fa fa-angle-right arrow-slider-mb"></i>'],
         margin: 20,
         center: false,
     });


});

$(document).ready(function () {
    var $countdownPc = $('.countdown-pc');
    var $countdownMb = $('.countdown-mb');
    var date = new Date("July 15, 2020 02:15:00"); //Month Days, Year HH:MM:SS
    var now = new Date();
    var diff = (date.getTime()/1000) - (now.getTime()/1000);
    var clock = $countdownPc.FlipClock(diff,{
        clockFace: 'DailyCounter',
        countdown: true
    });
    var clock = $countdownMb.FlipClock(diff,{
        clockFace: 'DailyCounter',
        countdown: true
    });


    console.log(date);
    $('.btn-viewed').click(function () {
        $('.product_viewed').toggleClass('show');
        $(this).toggleClass('active');
    })
    $('.close-viewed').click(function () {
        $('.product_viewed').removeClass('show');
    })

    overlay3 =$("<div class='overlay'></div> ");
    $( "body" ).prepend(overlay3);

    $('.addCart').click(function () {
        $('.widget-cart').toggleClass('show');
        overlay3.toggle();
        return false;
    })

    $('.close-cart').click(function () {
        $('.widget-cart').removeClass('show');
        overlay3.hide();
        return false;
    })
    overlay3.click(function(){
        $('.close-cart').trigger('click');
    })
    if ($('.widget-cart .prod-cart').length) {
        $('.cart-null').remove();
    }
});
